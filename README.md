#### brought to you by
# MedinaDev.com 
#### Simple Tutorials | Clean Reusable Code Snippets
<br />
<br />

# Purpose of this project
To give you a quick/easy way to create a new typescript node project!
- copy project from: https://gitlab.com/medinadev.com.tutorials/easy-typescript-starter.git and rename to fit your needs!
  

# How to run this project?
In root directory, run the following commands:
1. ```npm install ```
2. ```npm run build ```
3. ```npm start ```

You should see the following console output:
```sh
node ./dist/main.js

progam start..
progam end..

Terminal will be reused by tasks, press any key to close it.
```

# What is this project doing?
1. ```npm install ``` -- creates node modules folder and installs dependencies
2. ```npm build ```   -- complies project typescript into javascript and places the .js files in /dist
  <br /> NOTE: This requires typescript to be installed, you can install it like so:```run npm install -g typescript```
1. ```npm start ``` -- starts the program by running ``node ./dist/main.js``
   
# How can I reuse this project?
1. rename project to whatever you like
2. change package.json "name" field to represent your project
3. start coding in main.ts!


# What else is included in this project?
### Node Types 
    Node types were installed to allow your typescript classes to easily use node libraries - defined in package.json
### .gitignore
    Added so you don't accidentally commit and push the /node_modules folder when you decide to use this project in your git branch
   
